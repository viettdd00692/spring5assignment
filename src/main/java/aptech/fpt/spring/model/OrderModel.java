package aptech.fpt.spring.model;

import aptech.fpt.spring.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderModel extends PagingAndSortingRepository<Order, Integer> {
    Page<Order> findOrdersByStatus(Integer status, Pageable pageable);
}
