package aptech.fpt.spring.model;

import aptech.fpt.spring.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
