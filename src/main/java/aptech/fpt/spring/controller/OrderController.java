package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.Order;
import aptech.fpt.spring.model.OrderModel;
import aptech.fpt.spring.model.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Controller
public class OrderController {

    @Autowired
    private OrderModel orderModel;
    @Autowired
    private OrderRepository orderRepository;

    @RequestMapping(path = "/order/list", method = RequestMethod.GET)
    public String getListOrder(Model model, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<Order> pagination = orderModel.findOrdersByStatus(1, PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        model.addAttribute("datetime", Calendar.getInstance().getTime());
        return "order-list";
    }

    @RequestMapping(value = "/order/add")
    public String addOrder(Model model) {
        model.addAttribute("order", new Order());
        return "order-form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(Order order) {
        orderRepository.save(order);
        return "redirect:/";
    }

    @RequestMapping(value = "/order/edit", method = RequestMethod.GET)
    public String editOrder(@RequestParam("id") Integer orderId, Model model) {
        Optional<Order> orderEdit = orderRepository.findById(orderId);
        orderEdit.ifPresent(order -> model.addAttribute("order", order));
        return "order-edit";
    }

    @RequestMapping(value = "/order/delete", method = RequestMethod.GET)
    public String deleteOrder(@RequestParam("id") Integer orderId, Model model) {
        orderRepository.deleteById(orderId);
        return "redirect:/";
    }

}
